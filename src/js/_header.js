const navBar = document.querySelector('.navbar');
const hamburger = document.querySelector('.hamburger');
const navMenu = document.querySelector('.nav-menu');

hamburger.addEventListener('click', function (event) {
    hamburger.classList.toggle('hamburger--active');
    renderNavMenu()
});

window.addEventListener('resize', renderNavMenu);

function renderNavMenu () {
    const w = window.innerWidth;
    if (w <= 480) {
        hamburger.classList.contains('hamburger--active') ? navMenu.style.display = 'block' : navMenu.style.display = 'none';
        const coordsNavBar = navBar.getBoundingClientRect();
        const coordsNavMenu = navMenu.getBoundingClientRect();
        navMenu.style.top = coordsNavBar.bottom + 'px';
    } else {
        navMenu.style.display = '';      
    }
};